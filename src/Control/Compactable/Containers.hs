{-# LANGUAGE LambdaCase #-}

module Control.Compactable.Containers where

import           Control.Compactable
import           Data.Monoid         ((<>))
import           Data.Set            as Set

instance Compactable Set where
  compact = foldl' (\b -> \case Just x -> Set.fromDistinctAscList $ Set.elems b ++ [x]; Nothing -> b) Set.empty
