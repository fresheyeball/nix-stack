{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation, base, compactable, containers, stdenv }:
      mkDerivation {
        pname = "nix-stack";
        version = "0.1.0.0";
        src = ./.;
        libraryHaskellDepends = [ base compactable containers ];
        license = stdenv.lib.licenses.bsd3;
      };

  compactable-src = pkgs.fetchgit
      { url = "https://gitlab.com/fresheyeball/Compactable";
        sha256 = "10rf1hqknvssyv0b6mqvghjwzhscf2iagj47wqrixj6w0h9b0p2i";
        rev = "3f8ee1499c0e2b8a7af03b77d3ca3dea97e09e15";
      };


  haskellPackages = (if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler}).override
   {
    overrides = self: super: {
        compactable = pkgs.haskellPackages.callCabal2nix "compactable" compactable-src {};
      };
    };

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

  if pkgs.lib.inNixShell then drv.env else drv
